﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Elective_Bot
{
    class Slack
    {
        private class Webhook
        {
            [JsonProperty("name")]
            internal string name;
            [JsonProperty("enabled")]
            internal bool enabled;
            [JsonProperty("hook")]
            internal string hook;
        }

        public Slack(string message)
        {
            // Get the Slack webhooks from a file
            Webhook[] webhooks;
            string webhookLocation = "webhooks.json";
            if (File.Exists(webhookLocation))
            {
                // Parse the stored webhooks
                string storedString = File.ReadAllText(webhookLocation);
                webhooks = JsonConvert.DeserializeObject<Webhook[]>(storedString);
            }
            else
            {
                Console.WriteLine("Webhook file does not exist, creating...");

                // Create the example hook
                Webhook hook = new Webhook();
                hook.name = "Example";
                hook.hook = "https://hooks.slack.com/services/";
                Webhook[] hooks = { hook };

                // Store the example hook
                string newStoredString = JsonConvert.SerializeObject(hooks);
                File.WriteAllText(webhookLocation, newStoredString);

                // Do not post a Slack message as there are no webhooks to use
                return;
            }

            // Format as a slack message
            SlackMessage mess = new SlackMessage(message);
            string messString = JsonConvert.SerializeObject(mess);

            // Go through all webhooks
            foreach (Webhook webhook in webhooks)
            {
                try
                {
                    if (webhook.enabled)
                    {
                        // Create request
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(webhook.hook);
                        request.KeepAlive = false;
                        request.ProtocolVersion = HttpVersion.Version10;
                        request.Method = "POST";

                        // Convert to byte stream
                        byte[] postBytes = Encoding.ASCII.GetBytes(messString);

                        request.ContentType = "application/json";
                        request.ContentLength = postBytes.Length;
                        Stream requestStream = request.GetRequestStream();

                        // Send
                        requestStream.Write(postBytes, 0, postBytes.Length);
                        requestStream.Close();

                        // Ignoring potential response
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        //Console.WriteLine(new StreamReader(response.GetResponseStream()).ReadToEnd());
                        //Console.WriteLine(response.StatusCode);
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            Console.WriteLine(string.Format("Failed to send Slack message to {0}", webhook.name));
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("Failed to connect to send Slack message to {0}", webhook.name));
                }
            }
        }
    }
}
