﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;

namespace Elective_Bot
{
    class SectionTally
    {
        private static readonly string REQUEST = "https://banner.rowan.edu/reports/reports.pl?task=Section_Tally";

        public static readonly string TERM_SPRING_2020 = "202020";
        public static readonly string TERM_FALL_2019 = "201940";
        public static readonly string TERM_SUMMER_2019 = "201930";
        public static readonly string TERM_SPRING_2019 = "201920";

        public static readonly string COLLEGE_ENGINEERING = "EN";

        public static readonly string[] ECE_CLASSES =
        {
            "09402",
            "09403",
            "09404",
            "09405",
            "09406",
            "09407",
            "09408",
            "09409",
            "09411",
            "09412",
            "09413",
            "09421",
            "09422",
            "09423",
            "09425",
            "09430",
            "09431",
            "09432",
            "09444",
            "09452",
            "09453",
            "09454",
            "09455",
            "09456",
            "09457",
            "09466",
            "09468",
            "09469",
            "09471",
            "09471",
            "09473",
            "09481",
            "09482",
            "09483",
            "09484",
            "09485",
            "09486",
            "09490",
            "09495"
        };

        public static string getCurrentTerm()
        {
            string term;

            if (DateTime.UtcNow.Month >= 9)
            {
                term = string.Format("{0}{1}", DateTime.UtcNow.Year + 1, 20);
            }
            else
            {
                term = string.Format("{0}{1}", DateTime.UtcNow.Year, 40);
            }

            return term;
        }

        public static ClassItem[] getClasses(string term, string college)
        {
            // Create the request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(REQUEST);
            string postData = string.Format("term={0}&task=Section_Tally&coll={1}&dept=ALL&subj=ALL&ptrm=ALL&sess=ALL&prof=ALL&attr=ALL&camp=M&bldg=ALL&Search=Search", term, college);
            byte[] data = Encoding.ASCII.GetBytes(postData);

            // Configure the request
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            // Send and download the response
            Stream stream = request.GetRequestStream();
            stream.Write(data, 0, data.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            // Get the table rows of the returned webpage
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(responseString);
            HtmlNodeCollection rows = doc.DocumentNode.SelectNodes("/html/body/table/tbody/tr");

            // Dump data into array
            ArrayList classes = new ArrayList();
            foreach (HtmlNode row in rows)
            {
                // Actual class row
                if (row.GetAttributeValue("style", "").Equals("background-color:inherit"))
                {
                    ClassItem c = new ClassItem();
                    HtmlNodeCollection cols = row.SelectNodes("td");

                    c.crn = cols[0].ChildNodes["a"].InnerHtml.Trim();
                    c.subject = cols[1].InnerHtml.Trim();
                    c.course = cols[2].InnerHtml.Trim();
                    c.section = cols[3].InnerHtml.Replace("&nbsp;", "").Trim();
                    c.partOfTerm = cols[4].InnerHtml.Replace("&nbsp;", " ").Trim();
                    c.session = cols[5].InnerHtml.Trim();
                    c.title = cols[6].InnerHtml.Trim();
                    c.professor = cols[7].InnerHtml.Trim();
                    c.time = cols[8].InnerHtml.Trim();
                    c.campus = cols[9].InnerHtml.Trim();
                    c.hours = cols[11].InnerHtml.Replace("&nbsp;", "").Trim();
                    c.maxSpots = cols[12].InnerHtml.Trim();
                    c.maxReservedSpots = cols[13].InnerHtml.Trim();
                    c.leftReservedSpots = cols[14].InnerHtml.Trim();
                    c.enrolledStudents = cols[15].InnerHtml.Trim();
                    c.availableSpots = cols[16].InnerHtml.Trim();
                    c.waitCapacity = cols[17].InnerHtml.Trim();
                    c.waitCount = cols[18].InnerHtml.Trim();
                    c.waitAvailableSpots = cols[19].InnerHtml.Trim();
                    c.roomCapacity = cols[20].InnerHtml.Trim();

                    classes.Add(c);
                }
            }

            return (ClassItem[])classes.ToArray(typeof(ClassItem));
        }
    }
}
