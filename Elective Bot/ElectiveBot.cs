﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Elective_Bot
{
    class ElectiveBot
    {
        static void Main(string[] args)
        {
            new ElectiveBot();
        }

        public ElectiveBot()
        {
            string currentTerm = SectionTally.getCurrentTerm();
            string electiveLocation = string.Format("electives{0}.json", currentTerm);

            ClassItem[] classes = SectionTally.getClasses(currentTerm, SectionTally.COLLEGE_ENGINEERING);

            // Look for current electives
            List<ClassItem> currentElectives = new List<ClassItem>();
            foreach (ClassItem c in classes)
            {
                // ME
                if (c.course.StartsWith("104"))
                {
                    //currentElectives.Add(c);
                }

                // ECE
                foreach (string eceClass in SectionTally.ECE_CLASSES)
                {
                    if (c.course.Equals(eceClass))
                    {
                        currentElectives.Add(c);
                    }
                }
            }

            StringBuilder slackMessage = new StringBuilder();
            bool shouldSend = false;
            if (File.Exists(electiveLocation))
            {
                // Parse the stored electives
                string storedString = File.ReadAllText(electiveLocation);
                List<ClassItem> storedElectives = JsonConvert.DeserializeObject<List<ClassItem>>(storedString);

                // Find new electives
                List<ClassItem> newElectives = new List<ClassItem>();
                foreach (ClassItem c in currentElectives)
                {
                    bool found = false;

                    foreach (ClassItem s in storedElectives)
                    {
                        if (s.crn.Equals(c.crn) && s.title.Equals(c.title))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        newElectives.Add(c);
                    }
                }

                if (newElectives.Count > 0)
                {
                    slackMessage.Append("<!channel> *_New_ Electrical and Computer Engineering Electives:*\n");

                    foreach (ClassItem c in newElectives)
                    {
                        slackMessage.Append(formatClass(c));
                    }

                    shouldSend = true;
                }

                // Find changed electives
                List<ClassItem> changedElectives = new List<ClassItem>();
                int numClassesReopened = 0;
                foreach (ClassItem c in currentElectives)
                {
                    bool found = false;

                    //Console.WriteLine("s");
                    foreach (ClassItem s in storedElectives)
                    {
                        //Console.WriteLine(s.crn);
                        if (s.crn.Equals(c.crn))
                        {
                            try
                            {
                                int ce = int.Parse(c.enrolledStudents);
                                int cm = int.Parse(c.maxSpots);
                                int se = int.Parse(s.enrolledStudents);
                                int sm = int.Parse(s.maxSpots);

                                if ((se == sm) && ((cm > sm) || (ce < se)))
                                {
                                    numClassesReopened += cm - ce;
                                }
                            }
                            catch (Exception e)
                            {
                                // Oh fuck, but don't care
                            }

                            if (!s.enrolledStudents.Equals(c.enrolledStudents))
                            {
                                found = true;
                                break;
                            }
                            if (!s.availableSpots.Equals(c.availableSpots))
                            {
                                found = true;
                                break;
                            }
                            if (!s.maxSpots.Equals(c.maxSpots))
                            {
                                found = true;
                                break;
                            }
                            if (!s.professor.Equals(c.professor))
                            {
                                found = true;
                                break;
                            }
                            if (!s.time.Equals(c.time))
                            {
                                found = true;
                                break;
                            }
                            if (!s.title.Equals(c.title))
                            {
                                found = true;
                                break;
                            }
                        }
                    }

                    if (found)
                    {
                        changedElectives.Add(c);
                    }
                }

                if (changedElectives.Count > 0)
                {
                    if (newElectives.Count > 0)
                    {
                        slackMessage.Append("\n\n");
                    }

                    if (numClassesReopened == 1)
                    {
                        slackMessage.AppendFormat("<!channel> _*1 Elective Seat Opened*_\n");
                    }
                    else if (numClassesReopened > 1)
                    {
                        slackMessage.AppendFormat("<!channel> _*{0} Elective Seats Opened*_\n", numClassesReopened);
                    }

                    slackMessage.Append("*_Changed_ Electrical and Computer Engineering Electives:*\n");

                    foreach (ClassItem c in changedElectives)
                    {
                        slackMessage.Append(formatClass(c));
                    }

                    shouldSend = true;
                }
            }
            else
            {
                // Output the list of electives
                slackMessage.Append("*Electrical and Computer Engineering Electives:*\n");

                foreach (ClassItem c in currentElectives)
                {
                    slackMessage.Append(formatClass(c));
                }

                shouldSend = true;
            }

            // Send the slack message
            if (shouldSend)
            {
                //Console.WriteLine(slackMessage.ToString());
                new Slack(slackMessage.ToString());
            }

            // Store the new electives
            string newStoredString = JsonConvert.SerializeObject(currentElectives);
            File.WriteAllText(electiveLocation, newStoredString);

            // Combine the electives
            //string slackMessage = string.Format("{0}\n\n{1}", ece, me);

            // Send the electives
            //Console.WriteLine(slackMessage);
            //new Slack(slackMessage.ToString());
            //new Slack("<!channel> oh, and here's David: <@U7SNS1QV8>");
        }

        private string formatClass(ClassItem c)
        {
            string o = string.Format("\n- _*{0}*_ with _{1}_. {2} of {3} spots filled. CRN {4}.",
                        System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(c.title.ToLower()),
                        c.professor, c.enrolledStudents, c.maxSpots, c.crn).Replace("..", ".");
            if ((c.time != null) && !string.IsNullOrEmpty(c.time))
            {
                o += string.Format("\n    {0}", c.time.Replace("<br>", "\n    "));
            }
            return o;
        }
    }
}
