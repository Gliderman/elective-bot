﻿using Newtonsoft.Json;

namespace Elective_Bot
{
    class SlackMessage
    {
        [JsonProperty("text")]
        public string message { get; set; }

        public SlackMessage(string message)
        {
            this.message = message;
        }
    }
}
