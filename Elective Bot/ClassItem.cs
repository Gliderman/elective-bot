﻿using Newtonsoft.Json;

namespace Elective_Bot
{
    class ClassItem
    {
        [JsonProperty("crn")]
        public string crn;
        [JsonProperty("sub")]
        public string subject;
        [JsonProperty("crs")]
        public string course;
        [JsonProperty("sec")]
        public string section;
        [JsonProperty("pot")]
        public string partOfTerm;
        [JsonProperty("ses")]
        public string session;
        [JsonProperty("tit")]
        public string title;
        [JsonProperty("pro")]
        public string professor;
        [JsonProperty("tim")]
        public string time;
        [JsonProperty("cam")]
        public string campus;
        [JsonProperty("hrs")]
        public string hours;
        [JsonProperty("max")]
        public string maxSpots;
        [JsonProperty("mar")]
        public string maxReservedSpots;
        [JsonProperty("ler")]
        public string leftReservedSpots;
        [JsonProperty("enr")]
        public string enrolledStudents;
        [JsonProperty("ava")]
        public string availableSpots;
        [JsonProperty("wca")]
        public string waitCapacity;
        [JsonProperty("wco")]
        public string waitCount;
        [JsonProperty("wav")]
        public string waitAvailableSpots;
        [JsonProperty("cap")]
        public string roomCapacity;

        public override string ToString()
        {
            return string.Format("{0} Course: {1}, Section: {2}, CRN: {3}", title, course, section, crn);
        }
    }
}
